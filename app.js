var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors');

var examRouter = require('./routes/exam');
var examRouterApi = require('./routes/api');
var userRouter = require('./routes/user');

//db Connection for moongoose
const mongoose = require("mongoose");
mongoose.connect(process.env.DB_CONNECTION,{ useUnifiedTopology: true, useNewUrlParser: true } ,() => {console.log("connected to mongoDB");});

var app = express();

app.listen(3001, () => {
    console.log("Server is listening on port: 3001");
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(cors());

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//Initiating routes
app.use('/api', examRouterApi);
app.use('/exams', examRouter);
app.use('/users', userRouter);

/* GET home page. */
app.get('/', function(req, res, next) {
    res.render('index', { });
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error/error');
});

module.exports = app;

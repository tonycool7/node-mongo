const express = require("express");

require('dotenv/config');

const examRouter = express.Router();

//Mongo db connection details
let mongodb = require('mongodb');
let client = mongodb.MongoClient;
let url= process.env.DB_CONNECTION;

let connection = client.connect(url, { useUnifiedTopology: true });

/* GET create exam form. */
examRouter.get('/create', (req, res, next) => {
    res.render('exams/create', { });
});

/* Post create exam. */
examRouter.post('/create', async (req, res, next) => {
    try{
        let con = await connection;
        let database = con.db("exam_sys");
        let result = await database.collection("exams").insert(req.body);
        res.redirect("/exams");
    }catch (e) {
        console.log(e);
    }
});

/* delete an exam. */
examRouter.post('/delete', async (req, res, next) => {
    try{
        let con = await connection;
        let database = con.db("exam_sys");
        let result = await database.collection("exams").deleteOne({ name : req.body.name});
        res.redirect("/exams");
    }catch (e) {
        console.log(e);
    }
});

examRouter.get('/', async (req, res, next) => {
    try{
        let con = await connection;
        let database = con.db("exam_sys");
        let result = await database.collection("exams").find({}).toArray();
        res.render('exams/index', { title: 'Exams', exams : result});
    }catch (e) {
        console.log(e);
    }
});

examRouter.get('/api/exams', async (req, res, next) => {
    try{
        let con = await connection;
        let database = con.db("exam_sys");
        let result = await database.collection("exams").find({}).toArray();
        res.json({exams : result});
    }catch (e) {
        res.json({error : e});
    }
});

module.exports = examRouter;

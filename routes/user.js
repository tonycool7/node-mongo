const express = require("express");

require('dotenv/config');

const userRouter = express.Router();

//User Model
const userModel = require("../models/Users");

//Registration form
userRouter.get('/register', (req, res, next) => {
    res.render('user/register');
});

//Display's list of all users
userRouter.get('/', async (req, res, next) => {
    const users = await userModel.find();
    res.render('user/index', {users});
});


//Processes registration form and adds user to platform
userRouter.post('/register', async (req, res, next) => {
    const user = new userModel({
       fname : req.body.fname,
       lname : req.body.lname,
       email : req.body.email
    });

    let result = await user.save();
    res.redirect('/users');
});

module.exports = userRouter;

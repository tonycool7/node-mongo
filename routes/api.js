const express = require("express");

require('dotenv/config');

const examRouterApi = express.Router();

//Mongo db connection details
let mongodb = require('mongodb');
let client = mongodb.MongoClient;
let url= process.env.DB_CONNECTION;

let connection = client.connect(url, { useUnifiedTopology: true });

examRouterApi.get('/v1/exams', async (req, res, next) => {
    res.setHeader('Content-Type', 'application/json');

    try{
        let con = await connection;
        let database = con.db("exam_sys");
        let result = await database.collection("exams").find({}).toArray();
        res.json(result);
    }catch (e) {
        res.json({error : e});
    }
});

module.exports = examRouterApi;

const mongoose = require("mongoose");
const validator = require("validator");

const UserSchema = mongoose.Schema({
    fname : {
        type: String,
        required: true,
    },
    lname : {
        type: String,
        required: true,
    },
    email : {
        type: String,
        required: true,
        unique: true,
        lowercase: true,
        validate: (value) => {
            return validator.isEmail(value)
        }
    },
    role : {
        type: String,
        default : "student"
    },
    password : {

    },
    updated_at : {
        type : Date,
        default: Date.now()
    },
    created_at : {
        type : Date,
        default: Date.now()
    }
});

module.exports = mongoose.model("Users", UserSchema);
